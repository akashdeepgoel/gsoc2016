# Google Summer of Code 2016
The development team of the [C3G](http://computationalgenomics.ca/) Montreal node is aiming to participate as an organization in Google Summer of Code 2016. We are offering to mentor students who wish to spend their summer working on interesting and user-friendly software pipelines for the analysis of Next-Generation Sequencing data. This page hosts information for prospective GSoC students and our project ideas. We are looking for 2 to 4 excellent students to collaborate on some of our research and development projects.

[**See GSoC 2016 full time line**](timeline.md)

--------------
Table of content:

[TOC]

--------------
# The CG3 montreal node 
The Montreal C3G nodes is hosted at the McGill University and Genome Quebec Innovation Center (MUGQIC). The Montreal node is strongly involved in the [GenAP](https://genap.ca/) developpement team and had developped a robust genomic data analysis pipeline set. Since 2011, we have completed more than 400 bioinformatics analysis projects with over 290 distinct groups of researchers across Canada. Our teams have significant experience in personalized medicine applications. These have included genome analysis and interpretation of personal genomes, technology and services to record patient presentations, DNA-, RNA- and ChIP-seq data analysis, and analysis of complete human epigenomes in both germline disorders and cancers.

The current members of the team are:

 * Guillaume Bourque, _Ph.D_ - Director
 * Mathieu Bourgey, _Ph.D_ - R&D Bioinformatics manager
 * Francois Lefebvre, _MSc_ - Services Bioinformatics manager
 * Edouard Henrion, _MSc_ - Software developer
 * David Bujold, _MSc_ - GenAP project manager
 * Catherine Cote, _MSc_ - Web designer
 * Robert Eveleigh, _MSc_ - Bioinformatics specialist
 * Gary Leveque, _MSc_ - Bioinformatics specialist
 * Johanna Sandoval, _MSc_ - Bioinformatics consultant
 * Pascale Marquis, _MSc_ - Bioinformatics specialist
 * Toby Hocking, _Ph.D_ - Postdoctoral researcher
 * Maiko Narahara, _Ph.D_ - Postdoctoral researcher
 * Jean Monlong, _MSc_ - PhD student
 * Patricia Goerner-Potvin, _MSc_ - PhD student
 * David Venuto, Master student
 * Joe Su, Master Student 

Collaborators:

 * Simon Gravel, _Ph.D_ - Assistant Professor at McGill University - [Simon's lab](simongravlabel.lab.mcgill.ca/Home.html)

# Proposed Projects 

## Flowchart creator for MUGQIC Pipelines
[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution. Job commands and parameters can be modified through several configuration files. 

We actually maintain 7 different pipelines and currently develop 3 others. Each pipeline contains between 10 to 40 steps. The development of these pipelines are in constant evolution and moreover users can decide to only run a selection of steps from the pipeline. Thus having an integrated system that automatically builds the  flowchart of the steps will be a user-friendly add-on to our software. 

The goal of this project is to develop the flowchart creation engine in the pipeline python object of our software.

### Developer profile

Required skills: Python, git.

Nice to have: Experience in flowchart development.

### Selection tests
TBD

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)

[Johanna Sandoval](<johanna.sandoval@computationalgenomics.ca>)

-------------------------------

## Implement base modification analysis in the pacbio_assembly pipeline from MUGQIC Pipelines
[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution. 

Among the different pipelines we maintain, one of them is decidated to automate the generation of genomic assembly when sufficient coverage (>= 100x) of PacBio genomic data is available for a given organism. In summary, contigs assembly with PacBio reads is done using what is refer as the [HGAP workflow](https://github.com/PacificBiosciences/Bioinformatics-Training/wiki/HGAP-2.0). Briefly, raw subreads generated from raw .ba(s|x).h5 PacBio data files are filtered for quality. A subread length cutoff value is extracted from subreads, depending on subreads distribution, and used into the preassembly (aka correcting step) ([BLASR](https://github.com/PacificBiosciences/blasr)) step which consists of aligning short subreads on long subreads. Since errors in PacBio reads is random, the alignment of multiple short reads on longer reads allows to correct sequencing error on long reads. These long corrected reads are then used as seeds into assembly ([Celera assembler](https://sourceforge.net/projects/wgs-assembler/)) which gives contigs. These contigs are then polished by aligning raw reads on contigs (BLASR) that are then processed through a variant calling algorithm ([Quiver](https://github.com/PacificBiosciences/GenomicConsensus/blob/master/doc/HowToQuiver.rst)) that generates high quality consensus sequences using local realignments and PacBio quality scores. 

Once the assembly is generated we can use this sequence to reprocess the raw data in order to estimate the base modification pattern of the sample ([see Article](https://nar.oxfordjournals.org/content/early/2011/12/07/nar.gkr1146.full). To do this the PacBio developement team released the tool [MotifMaker](https://github.com/PacificBiosciences/MotifMaker). The goal of this project is to integrate the base modification analysis (using MotifMaker) in our pipeline while following our developpement standard.


### Developer profile

Required skills: Python, git.

Nice to have: Experience in Genomics and Pacbio data.

### Selection tests
TBD

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)

[Gary Leveque](<gary.leveque@computationalgenomics.ca>)

------------------------------------

## Integrate structural variants calls in the tumor_pair pipeline from MUGQIC Pipeline  
[MUGQIC pipelines](https://bitbucket.org/mugqic/mugqic_pipelines) consist of Python scripts which create a list of jobs running Bash commands. Those scripts support dependencies between jobs and smart restart mechanism if some jobs fail during pipeline execution. 

Among the different pipelines in developement, we currently working on a pipeline for cancer oriented genomic analysis. The tumor_pair pipeline is based on our DNA-Seq pipeline and follow its first initial steps. The standard MUGQIC DNA-Seq pipeline uses [BWA](http://bio-bwa.sourceforge.net/) to align reads to the reference genome. Treatment and filtering of mapped reads approaches as Indel realignment, mark duplicate reads, recalibration and sort are executed using [Picard](broadinstitute.github.io/picard/) and [GATK](https://www.broadinstitute.org/gatk/). The difference starts at the variant calling step. At this point, a paired caller is used to call Single Nucleotide Variations and Indels.

The goal of this project is to implement additional steps in the pipeline in order to integrate Structural Variant detection in the analysis. This tasks will focus on the integration of software like [Lumpy](https://github.com/arq5x/lumpy-sv), [Delly](https://github.com/tobiasrausch/delly), [SCoNEs](https://bitbucket.org/mugqic/scones), [PopSV](https://github.com/jmonlong/PopSV), and others in our pipeline while following our development standard. 

### Developer profile

Required skills: Python, git.

Nice to have: Experience in Cancer Genomics.

### Selection tests
TBD

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>) / [Edouard Henrion](<edouard.henrion@computationalgenomics.ca>)

[Robert Eveleigh](<robert.eveleigh@computationalgenomics.ca>)
 
-----------------------------

## Improve SegAnnDB interactive genomic segmentation web app

### Rationale

[SegAnnDB](http://bioviz.rocq.inria.fr) is a web app for interactive genomic segmentation of DNA copy number profiles, [published in Bioinformatics](http://bioinformatics.oxfordjournals.org/content/early/2014/02/03/bioinformatics.btu072.short ). It combines previous work on data visualization, computer vision, and machine learning into a web site that uses annotated region labels to build a user-specific segmentation model. YouTube videos explain how it works: [basic labeling](https://www.youtube.com/watch?v=BuB5RNASHjU), [labeling and exporting high-density profiles](https://www.youtube.com/watch?v=al0kk1JWsr0). The goal of this project is to add advanced rendering and social features to SegAnnDB.

### Approach

The ideal student project would propose to work on some items from the [TODO list](https://github.com/tdhock/SegAnnDB/blob/master/NEWS_TODO.org):

* Add support for browsers that do not render large PNG images. SegAnnDB currently uses very large PNGs to efficiently visualize DNA copy number profiles, but these are not supported by all browsers and [you can test your browser's support on this web page](http://sugiyama-www.cs.titech.ac.jp/~toby/images/). One way to fix this problem would be by providing links to sub-regions of chromosomes, and images that are never larger than, say, 1500 pixels wide.
* Add social features for sharing annotations. SegAnnDB currently lets a user login using Mozilla Persona and then add user-specific annotations. These annotations are currently only accessible to the user that creates them, but it would be nice to be able to share them with others. For example, Alice annotates some data then types the email address of her friend Bob into a web form. Bob then receives an email with a web link where he can view Alice's annoatations.
* Add unit tests using [Pyramid recommendations](http://docs.pylonsproject.org/en/latest/community/testing.html), for example when a profile is processed (plotter.db.Profile.process) test for presence of objects in the database, and then when the profile is deleted, check for deletion of relvant objects and files (PNG scatterplots, probes.bedGraph.gz data).

### Developer profile

Required skills: git, JavaScript, Python. 

Nice to have: experience with [D3](http://d3js.org/) and [Pyramid](http://www.pylonsproject.org/projects/pyramid/about) web framework.

### Code 

SegAnnDB is implemented as a Pyramid web app with a D3/JavaScript interface, [source code](https://github.com/tdhock/SegAnnDB).

### Selection tests

Install SegAnnDB on your local machine by following the instructions in [INSTALL.sh](https://github.com/tdhock/SegAnnDB/blob/master/INSTALL.sh). Make a YouTube video screencast where you upload and label some data sets on your local SegAnnDB server. 

### Mentor

Toby Hocking <toby.hocking@mail.mcgill.ca>

--------------------------------- 

## Develop a noise reduction engine for SCoNEs 
[SCoNEs](https://bitbucket.org/mugqic/scones) is a development tools in R language which aims to call Copy Number Variation in paired cancer data (whole genome sequeuncing) using a Read Depth (RD) approach. 

The specificity of SCoNEs is to incorporate the individual (biological) variations of the RD signal in order to adjust the set of paramter used by the calling algorithm. This apporach can suffers from the presence of a technical noise in the signal that could in a certain case leads to on overfitting of the RD signal. 

The goal of the project is to integrate an object that could be use to reduce the noise of the RD signal. We are acutally thinking about using a machine learning approach but we are open to any other efficient startegy.

### Developer profile

Required skills: R, git.

Nice to have: Experience in Cancer Genomics, Statistics, Machine Learning or Noise Reduction.

### Selection tests
TBD

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>)

Toby Hocking

--------------------------------- 

## Develop add-ons for SCoNEs
[SCoNEs](https://bitbucket.org/mugqic/scones) is a development tools in R language which aims to call Copy Number Variation in paired cancer data (whole genome sequeuncing) using a Read Depth (RD) approach. The specificity of SCoNEs is to incorporate the individual (biological) variations of RD signal in order to adjust the set of paramter of the calling algorithm. The actual SCoNEs project is still in developpement and several add-ons are still to be implemented. 

The goal of the project is  to integrate two majors developpements:
   
    1 To implement a visualization engine (interactive genomic viewer) which could be use to manually explore the results
    2 To write the software documentation.

Addtionaly other add-on sugestions and developements from the student will be welcome.

### Developer profile

Required skills: R, git.

Nice to have: Experience in Cancer Genomics and statistics.

### Selection tests
TBD

### Mentors
[Mathieu Bourgey](<mathieu.bourgey@computationalgenomics.ca>)

Jean Monlong


--------------------------------- 

## EGA Data Submission database (mEGAdata)

#### Description
mEGAdata is a database-driven lightweight web application for archiving genomics projects metadata on samples and experiments, with the goal of easing submission to the [European Genome-phenome Archive (EGA)](https://www.ebi.ac.uk/ega/home). 

The project has been developed in Python using the Flask microframework as a backend REST service, and Peewee as an ORM to a MySQL database. The front-end is coded in AngularJS, and uses the Handsontable library to give the interface an Excel-like data display that scientists are accustomed to.

It is used by the McGill Epigenomic Data Coordination for data submissions to EGA, following standards proposed by the International Human Epigenome Consortium (IHEC). For medium to large scale projects with more than a dozen datasets, submitting high-throughput sequencing experiments to the EGA can be tedious using the Webin interface. mEGAdata uses the EGA programmatic interface to submit samples in a more automated way.

Currently, the application supports:

* Donors and samples metadata through the web interface  
* Experiment (assay) and high-throughput sequencing runs metadata from the database
* SRA XML documents generation for high-throughput submissions

This GSoC project involves implementing new features to the mEGAdata application. Such features include:

* Developing new components to store and display metadata related to assays, sequencing runs and so on
* Expand existing database to support bioinformatics downstream analysis metadata
* Improve the application to make it easy to distribute, by switching to an SQLite database, and perhaps make it available as a Docker container 

This application will be made available as an open-source tool to assist any genomics project to submit their data to the EGA.


#### Required skills
Python, Javascript, SQL, git.

#### Keywords
Web application, Database, Python, Javascript, AngularJS, Flask, Peewee

![mEGAdata](img/mEGAdata.png)

#### Selection tests
You can do one or many of these steps. Reaching further steps makes you more likely to be selected.

* Step 1: Code a simple REST application using Flask that offers:
    * a POST method to add a name and a phone number to an object in memory
    * a GET method that returns a phone number for a given name, or the whole list of names/phone numbers. 
* Step 2: Make the PUT method to write to an SQLite database table instead of the in memory object.
* Step 3: Write a simple AngularJS application that displays all names and phone numbers provided by the REST service in an HTML table. 

#### Mentors
[David Bujold](<edcc@mail.mcgill.ca>)


--------------------------------- 

## Development of an HTML dynamic matrix to represent datasets with multiple dimensions 

The [IHEC Data Portal](http://epigenomesportal.ca/ihec) centralizes epigenomics datasets coming from multiple member consortia, by storing the relevant metadata in a relational database system.
To integrate datasets from 

[DIG Viewer](http://bitbucket.org/genap/digviewer) has been developed as part of this project, as an open-source library to create a versatile HTML grid to navigate through and select datasets with multiple dimensions. It makes use of the Javascript D3 library. You can see a simple usage example [here](http://epigenomesportal.ca/digviewer).

The proposed project project involves improving the library to add additional functions and provide smoother navigation. Such functions include:

* Expand/Collapse of rows / columns, making use of D3's transitions
* Making matrix rows and columns always visible when scrolling vertically or horizontally
* Updating grid content using transitions when data model gets updated

#### Required skills
Javascript, HTML, CSS

#### Keywords
Javascript, D3, HTML

#### Selection tests
You can do one or many of these steps. Reaching further steps makes you more likely to be selected.

* Step 1: Create an HTML document that uses D3 to display the following JSON document as a table with headers: [data](http://bl.ocks.org/mbostock/3887118#data.tsv).
* Step 2: Make that table sortable when clicking on headers.
* Step 3: Draw a pie chart displaying sepalWidth for versicolor species, falling in these categories: (<=2.5), (>2.5 and <=3), (>3 and <= 3.5), (>3.5 and <= 4), (>4).
* Step 4: Make this pie chart display the same data as in step 3, displaying 1 species every 5 seconds. Use D3 transitions when switching to another species.


### Mentors
[David Bujold](<edcc@mail.mcgill.ca>)
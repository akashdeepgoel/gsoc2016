# GSoC 2016 Proposal

## ***Project Info***

### Project Title

```Development of an HTML dynamic matrix to represent datasets with multiple dimensions ```

### Short Title

```DIG Viewer```

### URL

[Project Idea Page](https://bitbucket.org/mugqic/gsoc2016/overview#markdown-header-development-of-an-html-dynamic-matrix-to-represent-datasets-with-multiple-dimensions)

## ***Biographical Information***

I, ```Akashdeep Goel``` hail from a small town Nangal located on the foothills of shivaliks, Punjab, India. I did my schooling at DAV Public School and later on cracked **IIT JEE** with All India Rank of 537 (among 1.5 million candidates). I joined Computer Science and Engineering at IIT Roorkee in 2014 and have been involved in development since then. I came to know about [SDSLabs](https://sdslabs.co/) - leading technical group on campus during my high school and got selected after a rigorous selection process. I have a good experience in developing software in a team environment. We at SDSLabs, constantly strive to innovate and foster technical activities at IIT Roorkee. Some of our public projects are hosted on github: https://github.com/sdslabs/. I have contributed to many of their projects(both public and private) along with many other developers.

### Projects Done

- **Codevillage**

    - ***Application*** - Codevillage is an online coding platform for competitive  programmers to practice and take part in challenges held regularly. User needs to crack given algorithmic problem and submit the code that runs within constraints and gives correct output.Codevillage is hosted in a private repository due to security issues. It can be accessed at [https://codevillage.sdslabs.co/](https://codevillage.sdslabs.co/)
    - ***Role*** - As a lead developer of the application, I made compete backend and frontend of this app. Backend has been written in PHP using Laravel framework and pure CSS (for CSS part) and handlebar JS have been used as frontend tools. Grader and judge are written using C- language. Queuing system has been implemented using inbuilt laravel support. Major challenge was to have multiple submissions pushed which was solved using **redis** queues.
    - ***Recognition*** - Application won first prize in dynamic category at **Srishti** - Annual technical exhibition of IIT Roorkee.


- **Grader/Judge**

    - ***Application*** - Grader is an integral module of codevillage which takes output of CodeRunner and fetches constraints from database to judge a submission. Rules for grading are imported as a rule object and output is sent to database in form of status codes (like AC- Accepted).
    - ***Role*** - Wrote complete grader as a Laravel service and integrated it with codevillage. Grader is completely extensible and portable to multiple servers.


- **CodeRunner**

    - ***Application*** - Another core module of codevillage which compiles source code and runs it on the given input files to generate output. Output of this stage is fed as an input to Grader for judging the submissions. It takes arguments like memory-limit,time-limit to avoid infinite looping.
    - ***Role*** - Developed part for running c++ and c submissions along with another developer who made CodeRunner for running Java and python submissions.


- **Muzi Updater**

    - ***Application*** - This is a node.JS based application which automates the fetching of songs for **Muzi** (Internal Music Player for IIT Roorkee students) developed by SDSLabs.
    - ***Role*** - Developed complete node.JS script for parsing RSS feeds from Apple Itunes store and sending the data to ***Tornami*** - client for downloading songs (developed by SDSLabs).


- **CodeBuzz**

    - ***Application*** - Online judge made using Python Django framework as a part of course project. Code is hosted on my github profile at : [https://github.com/akashdeepgoel/codebuzz](https://github.com/akashdeepgoel/codebuzz)
    - ***Role*** - Developed the whole backend (in Django) and frontend (using bootstrap) of the application.


- **Package Hunt**

    - **Application** - Another node.JS based terminal app for developers to search libraries easily. Takes language and use purpose as input arguments and suggests libraries that can be used.Code is available at
    [https://github.com/akashdeepgoel/Package-Hunt](https://github.com/akashdeepgoel/Package-Hunt)
    - **Role** - Made complete app using Github API and various tolerance parameters as suggested by fellow members at SDSLabs (done in my first year).


- **Slack Bot Scripts**

    - Small scripts written in coffeescript language for bot integrated in SDSLabs slack channel.
    - I made two scripts, one for tracking birthdays of our members and other for status updates on train reservations using Indian Railways API.
    - Scripts can be seen at public SDSLabs repo : [https://github.com/sdslabs/bot](https://github.com/sdslabs/bot)


***Why I think I am qualified for the project?***
 - I have been actively involved in web development work since one and a half year and has got ample experience of good development environment. Also I have good experience of working with js libraries having done few projects exclusively on them. Besides this, I am a patient listener, avid learner and a highly dedicated person. I intend to maintain a dev log - where I would be updating my work everyday - for mentors to know easily what I am up to. All in all my perseverance and attitude qualifies me for this project.

## ***Contact Information***

| Student Name   | Melange Id            | Student Postal Address                                                  | Telephone(s)                      | Email(s)                                              | Skype          | Google+            |
|----------------|-----------------------|-------------------------------------------------------------------------|-----------------------------------|-------------------------------------------------------|----------------|--------------------|
| Akashdeep Goel | No Id given this time | H.NO 3-Q, BBMB COLONY NANGAL TOWNSHIP DISTT. ROPAR, PUNJAB PIN - 140124 | (+91) 9530920095 (+91) 7060334218 | akashdeep.goel1996@gmail.com adg96.ucs2014@iitr.ac.in | akashdeep.goel | akashdeep.goel1996 |

## ***Student Affiliation***

### Institution

```Indian Institute Of Technology, Roorkee```

### Program

4 year B.Tech, Computer Science & Engineering

### Stage of completion

Second Year (4th semester)

### Contact to verify

Dr. R Balasubramanian (Associate Professor at IIT Rooreke)
- [balaiitr@gmail.com](mailto:balaiitr@gmail.com)


## ***Schedule Conflicts***

No other engagements of any sort during Summer 2016.

## ***Mentors***

### Mentor Names
```David Bujold```

### Mentor e-mail
[david.bujold@computationalgenomics.ca](mailto:david.bujold@computationalgenomics.ca)

### Mentor link_id

[https://bitbucket.org/dbujold/](https://bitbucket.org/dbujold/)

### Have you been in touch with the mentors? When and how?

I have been in touch with the mentor right from the beginning of this project announcement and had good talks with him on bitbucket as well as through e-mail. I contacted him for selection tests and also informed about progress periodically. We had discussion over proposal and I took care to incorporate the changes that were suggested.


## ***Synopsis***

Project focusses on extending a DIG Viewer library based on **d3.js**. Features proposed in this GSoC projects are :-

- Expanding/Collapsing of rows/columns using d3.js transitions
    - Grouping of items based on categories
- Vertical/Horizontal scroll labels visibility
    - Labels must be visible when scrolling through rows/columns
- Dynamically updating grid using transitions whenever data model is updated
    - Modifying grid without losing current selection state of the grid
- Implement filtering based on functions
- Implement Partial cell content selection


## ***Benefits to Community***
IHEC Data portal has huge database which can be utilized for various research purposes if visualized properly.
This project will enhance the existing DIG viewer library and add much needed features to provide rich experience and smoother
user interface and navigation. This will help in analyzing huge data sets with ease and make it comfortable to get useful results.
Addition of dynamic updates to the DIG Viewer will help in monitoring and doing real-time analysis. All in all this project will help make DIG viewer an even better experience and more user-friendly.


## ***Coding Plan and Methods***
My Coding methodology will be based on **iterative** model of software development.  I personally feels repeated iterations will make things better and help in guided development of the project.

- Collapsing/Expanding rows & columns using d3.js transitions
    - Focus will be mainly on categorizing similar things based on example data and clipping the portion of  ***svg*** containing those rows. On expanding a transition from set of d3.js transitions will be used to make it visible.
    - A ***svg*** element containing '+' sign as image will show that this cell can be expanded.
    - Show/Hide property of d3.js will be used. Example usage can be seen here [http://bl.ocks.org/d3noob/5d621a60e2d1d02086bf](http://bl.ocks.org/d3noob/5d621a60e2d1d02086bf)
    - On expanding ***svg*** will be changed to '-' sign image.
    - Functionality can be controlled by using ```onClick()``` property of DOM element.


- Vertical/Horizontal scroll and labels visibility
    - One of the solution is to enclose ```.svg``` element in ```div``` and make dimensions of svg larger setting the overflow of the div to auto though there can be other options as well
    - Plan for this is already included in timeline
    - Another solution that I recently found is using virtual scroller d3.js plugin. Example usage is at [http://bl.ocks.org/billdwhite/36d15bc6126e6f6365d0](http://bl.ocks.org/billdwhite/36d15bc6126e6f6365d0)
    - This can be easily implemented


- Dynamically updating grid using transitions
    - Repeated ajax request with caching will be used so as to save time and memory for checking whether any data has changed or new data has come up
    - After checking the data changes via AJAX request. We can use d3.js methods to update ```svg``` dynamically.
    - An interesting article on this can be accessed at [http://www.d3noob.org/2013/02/update-d3js-data-dynamically-button.html](http://www.d3noob.org/2013/02/update-d3js-data-dynamically-button.html)


- Filtering based on provided function
    - There can be multiple solutions for this particular problem.
    - One of the solution is based on working of e-commerce websites in which we can make fresh requests based on parameters decided by Filtering followed by redrawing (might be expensive)
    - Another solution is to make ```svg classes``` according to pre-defined filtering functions and use css clipping properties.
    - A good solution can be to use d3.js ```filter``` function.
    - A nice example is given in D3.JS Tips and Tricks Book.



- Partial Cell content selection
    - Popup box can be displayed with content being fetched using AJAX request and then selection can be displayed using frontend rendering.


- Minor Improvements
    - Column based bracketing can be easily done using append property of d3.js by appending an ```svg``` element having bracket.


## ***Timeline***

I believe I have enough fuel to get started on my goals as a result to my involvement with the organization for almost one month now. So I will be setting grounds early to avoid stopgaps during the actual GSoC period. There are a couple of weeks before the actual timeline where I would make sure that I have done all the homework.

### Pre-Coding Phase
- During this period, I would try different methods of approaching a given solution and see the performance difference for various implementations. Work on expanding/collapsing of rows and categorization will start during this period itself

### 22 April - 29 April  (Community Bonding)
- Discussions with mentor so as to know each other better and also have clear understanding of code structure that organization prefers

### 1 May - 20 May (Categorization of cells)
- My vacations will start from 1st May and so, I will be able to devote around 8-10 hours a day on my project
- After having thorough understanding by now about the code structure and organization, I will start with coding phase and try to complete it in maximum of two weeks time.
- Testing phase for this module so that deliverable after this stage is perfect to be deployed with said feature.

### 21 May - 10 June (Scrollable Grid and labels visibility)

- I have already started on this particular problem as I have done some work related to this earlier.
- One of the solution is to enclose ```.svg``` element in ```div``` and make dimensions of svg larger setting the overflow of the div to auto.
- Comparing performance among two different implementations and finalizing the feature.

```
div {
  overflow : auto;
  height : //something greater than those of .svg
  width : //something greater than those of .svg
}
```

### 11 June - 25 June (Updating grid in realtime using transitions)

- This problem requires careful planning and lot of care as data can undergo various changes and may disappear in new update.
- I would probably propose more than one method for transitions and compare the relative performance and visual appeal for selecting the best one among them.
- Load testing using local backend and optimizing the algorithm so that product can be deployed complete with the feature.

### Milestone Reached - Mid Term Evaluation

### 28 June - 17 July (Filtering and partial content selection)
- Last few weeks of my vacations and so I will speed up the things so as to wrap up in time and would complete implementations of minor improvements.


### 17 July - Till End Term Evaluations

- Perfecting the partial content selection using pop-up box and also load testing it by rendering heavy data within pop-up box.

### **Contingency Plan**
Though, I would try my best to keep things on track as per the schedule but if due to any unforeseen circumstances, things fall out of place, My strategy would be to increase my working hours straightaway. Having no other engagements during summers, I won't face much difficulty in doing so.

## ***Management of Coding Project***

DIG Viewer library is hosted on bitbucket and I plan to contribute directly to the repository by working on separate branch for implementing different features. I would push the code and submit the pull request as soon as I am done with something new. I would also notify the mentor to kindly review and merge the pull request if it is compliant with the standards and fulfills the purpose.

## ***Testing***

### Why Test this Application?


Despite being mainly a frontend Project, It is really important to have thorough testing done.

- Minor CSS changes that throw things off
- Changes to JS files that break things
- Aggregates changing when not necessary
- Performance regressions

Following are the parameters on which testing needs to be done :

- Testing page load times
- Testing render speeds
- Sticking to a performance budget
- Verifying visual changes
- Accountability for code changes


### Functional Testing

***Casper JS***

Casper allows for scripted actions to be tested. It uses PhantomJS under the hood.

- Run the same test with multiple screen sizes.
- Test complex features or components.
- Automate complex user actions.
- Test content creation, transactions, other features.
- Keep an eye on problematic pages

### Performance Testing

***Automating Page Speed***

- Google's [Page Speed Insights](https://developers.google.com/speed/pagespeed/insights/) can be used as grading measure
- **Phantomas** is a web performance tool based on PhantomJS that can be used along with **grunt** to test the performance of the website

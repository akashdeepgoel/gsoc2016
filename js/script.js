var columns = ["serial","sepalLength","sepalWidth","petalLength","petalWidth","species"];
var titles = ["Sepal Length","Sepal Width","Petal Length","Petal Width","Species"];
var parsed;
var virginica = new Array(0,0,0,0,0);
var versicolor = new Array(0,0,0,0,0);
var setosa = new Array(0,0,0,0,0);
var count = 0;
var map = [];
map[0] = "virginica";
map[1] = "setosa";
map[2] = "versicolor";
d3.tsv("data.tsv", function(data) {
	parsed = data;

	var category = new Array(0,0,0,0,0);
	var i = 1;

	data.forEach(function(d) {

		d.serial=i;
		i++;
		var width = d.sepalWidth;
		var cat = catMap(width);
		if(d.species=="virginica")
		{
			virginica[cat]++;
		}
		else if(d.species=="versicolor")
		{
			versicolor[cat]++;
		}
		else
		{
			setosa[cat]++;
		}
	});
	
	fill(data,columns);
	
	drawPieChart();

});
function catMap(width){

	if(width<=2.5)
	{
		return 0;
	}
	else if(width>2.5 && width<=3.0)
	{
		return 1;
	}
	else if(width>3.0 && width<=3.5)
	{
		return 2;
	}
	else if(width>3.5 && width<=4.0)
	{
		return 3;
	}
	else if(width>4.0)
	{
		return 4;
	}
}

function fill(data,columns){

	var table = d3.select('table');
	var tbody = table.append('tbody');
	var thead = table.select('thead');
	var ascending = true;

	thead.append("tr")
         .selectAll("th")
         .data(columns)
         .enter()
         .append("th")
         .text(function(column) { return column; })
         .on('click', function (d) {
         	if(ascending)
         	{
         		ascending = false;
         		rows.sort(function (a, b) {
	         	if(d=="species")
	         	{
	         		var entry1 = a[d];
	         		var entry2 = b[d];
	         		return entry1.localeCompare(entry2);
	         	}
	         	else
	         	{
	         		return a[d]-b[d];
	         	}
	      		});
         	}
         	else
         	{
         		ascending = true;
         		rows.sort(function (a, b) {
	         	if(d=="species")
	         	{
	         		var entry1 = a[d];
	         		var entry2 = b[d];
	         		return entry2.localeCompare(entry1);
	         	}
	         	else
	         	{
	         		return b[d]-a[d];
	         	}
	      		});
         	}
         });

	var rows = tbody.selectAll("tr")
					.data(data)
					.enter()
					.append("tr");
	var cells = rows.selectAll("td")
					.data(function(row) { return columns.map(function(column) { return {column: column, value: row[column]}; }); })
					.enter()
					.append("td")
    				.html(function(d) { return d.value; });
	return table;
}

function drawPieChart(){

	var data = [{"label":"<=2.5","virginica":virginica[0],"setosa":setosa[0],"versicolor":versicolor[0]},
				{"label":">2.5 & <=3.0","virginica":virginica[1],"setosa":setosa[1],"versicolor":versicolor[1]},
				{"label":">3.0 & <=3.5","virginica":virginica[2],"setosa":setosa[2],"versicolor":versicolor[2]},
				{"label":">3.5 & <=4.0","virginica":virginica[3],"setosa":setosa[3],"versicolor":versicolor[3]},
				{"label":">4.0","virginica":virginica[4],"setosa":setosa[4],"versicolor":versicolor[4]}];

	var width = 730,
    height = 500,
    radius = Math.min(width, height) / 2;

	var color = d3.scale.category20();

	var pie = d3.layout.pie()
	    .value(function(d) { return d.virginica; })
	    .sort(null);

	var arc = d3.svg.arc()
	    .innerRadius(radius - 100)
	    .outerRadius(radius - 20);

	var svg = d3.select("#pie-chart").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	  .append("g")
	    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	  var path = svg.datum(data).selectAll("path")
	      .data(pie)
	    .enter().append("path")
	      .attr("fill", function(d, i) { return color(i); })
	      .attr("d", arc)
	      .each(function(d) { this._current = d; }); // store the initial angles

	  function change() {
	    count = (count+1)%3;
	    var spec = map[count];
	    document.getElementById("curr-specie").innerHTML = firstToUpperCase(spec);
	    pie.value(function(d) { return d[spec]; }); // change the value function
	    path = path.data(pie); // compute the new angles
	    path.transition().duration(750).attrTween("d", arcTween); // redraw the arcs
	  }

	function type(d) {
	  d.versicolor = +d.versicolor;
	  d.virginica = +d.virginica;
	  d.setosa = +d.setosa;
	  return d;
	}

	// Store the displayed angles in _current.
	// Then, interpolate from _current to the new angles.
	// During the transition, _current is updated in-place by d3.interpolate.
	function arcTween(a) {
	  var i = d3.interpolate(this._current, a);
	  this._current = i(0);
	  return function(t) {
	    return arc(i(t));
	  };
	}

	setInterval(function(){ 
   		change();
	},5000);
}

function firstToUpperCase( str ) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
}